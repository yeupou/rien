#!/usr/bin/perl
use strict;
use File::Basename;
use File::Path qw(make_path);
use File::Find;

my $curdir = $ARGV[0];
die "Invalid directory passed as argument" unless -d $curdir;

# get list of files that changed since the latest release
# number of commits
my $commits;
open(LATESTIS, "< $curdir/LATESTIS");
while (<LATESTIS>) { $commits = $_;}
close(LATESTIS);
chomp($commits);

# list of changed files
my %changed;
my %package_changed;
open(CHANGED, "git log --name-only -n $commits |");
while(<CHANGED>) {
    next if /^commit /;
    next if /^Author\: /;
    next if /^Date\: /;
    next if /^$/;
    next if /^\s/;
    # we dont need to be very selective since we just want a list of files
    chomp();
    $changed{"$curdir/$_"} = 1;
    # since now the tree match packages, we can already predict updated packages
    # (yeah, the script could be rethought completely)
    my ($topdir,) = split("/", $_);
    $package_changed{$topdir}++;
}
close(CHANGED);

# need also to get every directory within the path of updated files in the list
for my $file (keys %changed) {
    while ($file =~ /\//) {
	$file = dirname($file);
	$changed{$file} = 1;
	last if $file eq "/";
    }
}
print "Changed since $commits commit(s):\n  ";
for my $file (keys %changed) {
    print "$file ";
}
print "\n\n";
  
# get the list of packages
open(DEBCONTROL, "< $curdir/debian/control");
my @packages;
while (<DEBCONTROL>) { push(@packages, $1) if /^Package\:\s(.*)$/i; }
close(DEBCONTROL);

my $count;
sub wanted {
    my $dest = "$curdir/debian/rien-".substr($File::Find::name, length($curdir)+1, length($File::Find::name));   
    #print "$File::Find::name > $dest\n";
    
    # skip if exists already
    next if -e $dest;
    
    # create directory  755 mode per debian policy 
    if (-d $File::Find::name) {
	system("/usr/bin/mkdir",
	       "-p",
	       "--mode=755",
	       $dest);
	next;
    }
    
    # use cp for syminks, as install would not keep it as such
    if (-l $File::Find::name) {
	system("/usr/bin/cp",
	       "-a",
	       $File::Find::name,
	       $dest);
	next;
    }

    # plain text file
    #   755 mode per debian policy for executable
    #   644 otherwise
    if (-f $File::Find::name) {
	my $filemode = "644";
	$filemode = 755 if -x $File::Find::name;
	system("/usr/bin/install",
	       "--mode=$filemode",
	       $File::Find::name,
	       $dest);

	# record that this file changed
	$count++ if $changed{$File::Find::name};

	next;
    }	
}

open(NOTUPDATED, ">$curdir/debian/notupdated");
foreach my $pack (@packages) {
    my $updated = 0;
    print "Repacking package $pack:";
    $pack =~ s/^rien-//;

    # check if git say there is a file change and if there is force rebuild planed
    if (!$package_changed{$pack} and
	! -e "$curdir/debian/$pack.rebuild" and
	! -e "$curdir/debian/rebuild") {
    	print "\t=> no changes and no $pack.rebuild or rebuild instruction\n";
	# delete build dir
	system("rm", "-rf", "$curdir/debian/rien-$pack") if -d "$curdir/debian/rien-$pack";
	# register the information for later
	print NOTUPDATED "$pack\n";
	next;
    } 
    print " ...\n";
    # clone files
    $count = 0;
    find(\&wanted, "$curdir/$pack");
    unlink("$curdir/debian/$pack.rebuild") if -e "$curdir/debian/$pack.rebuild";
    print "  $count file(s) updated\n" if $count > 0;
}

close(NOTUPDATED);
unlink("$curdir/debian/rebuild") if -e "$curdir/debian/rebuild";




# EOF
