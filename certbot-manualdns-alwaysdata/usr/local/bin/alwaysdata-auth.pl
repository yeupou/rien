#!/usr/bin/perl
#
# Copyright (c) 2024 Mathieu Roy <yeupou--gnu.org>
#      http://yeupou.wordpress.com
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
#   USA

use strict;
use HTTP::Request;
use JSON::PP;
use LWP::UserAgent;
use Time::Piece;
use Config::IniFiles;

# require cerbot env                                                                                                       
for ("CERTBOT_DOMAIN", "CERTBOT_VALIDATION") {
    die "$_ environment variable must be set" unless $ENV{$_};
}

# require an ini file with apikey and account
my $cfgini = "/etc/letsencrypt/alwaysdata.ini";
my $cfg = Config::IniFiles->new(-file => $cfgini, -fallback => "GENERAL");
for ("apikey", "account", "domain") {
    die "$_ must be set in $cfgini, exiting" unless $cfg->val("GENERAL",$_);
}

# remove domain name (API wont allow it)
my $domain = $cfg->val("GENERAL","domain");
my $subdomain = "_acme-challenge.".$ENV{CERTBOT_DOMAIN};
$subdomain =~ s/\.$domain$//;

my $url = 'https://api.alwaysdata.com/v1/record/';
my $header = ['Content-Type' => 'application/json; charset=UTF-8'];
my $data = {    
    domain => '110165',
    name => $subdomain,
    value => $ENV{CERTBOT_VALIDATION},
    annotation => 'certbot '.localtime->strftime('%Y-%m-%d'),
    type => 'TXT',
};
my $encoded_data = encode_json($data);

my $request = HTTP::Request->new('POST', $url, $header, $encoded_data);
$request->authorization_basic($cfg->val("GENERAL","apikey"), "account=".$cfg->val("GENERAL","account"));

my $ua = LWP::UserAgent->new();
my $res = $ua->request($request);
die $res->message if $res->is_error;

# EOF 
