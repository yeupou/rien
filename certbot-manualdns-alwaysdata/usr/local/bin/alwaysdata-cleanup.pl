#!/usr/bin/perl
#
# Copyright (c) 2024 Mathieu Roy <yeupou--gnu.org>
#      http://yeupou.wordpress.com
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
#   USA

use strict;
use HTTP::Request;
use JSON;
use LWP::UserAgent;
use Config::IniFiles;

# require cerbot env
for ("CERTBOT_DOMAIN", "CERTBOT_VALIDATION") {
    die "$_ environment variable must be set" unless $ENV{$_};
}

# require an ini file with apikey and account
my $cfgini = "/etc/letsencrypt/alwaysdata.ini";
my $cfg = Config::IniFiles->new(-file => $cfgini, -fallback => "GENERAL");
for ("apikey", "account", "domain") {
    die "$_ must be set in $cfgini, exiting" unless $cfg->val("GENERAL",$_);
}

# remove domain name (API wont allow it)
my $domain = $cfg->val("GENERAL","domain");
my $subdomain = "_acme-challenge.".$ENV{CERTBOT_DOMAIN};
$subdomain =~ s/\.$domain$//;


# first get the dns record id
my $url = 'https://api.alwaysdata.com/v1/record/';
my $request = HTTP::Request->new('GET', $url);
$request->authorization_basic($cfg->val("GENERAL","apikey"), "account=".$cfg->val("GENERAL","account"));

my $ua = LWP::UserAgent->new();
my $res = $ua->request($request);
die $res->message if $res->is_error;

my $content =  decode_json($res->content);

my $recordid;
for (@{$content}) {
    # restrict to specific domain
    next unless  %{$_}{"name"} eq $subdomain;
    # and same validation
    next unless %{$_}{"value"} eq $ENV{CERTBOT_VALIDATION};
    # extract id
    $recordid = %{$_}{"id"};
    last;
}


# then delete it get the dns record id
# (will only delete on at a time)
my $url = "https://api.alwaysdata.com/v1/record/$recordid/";
my $request = HTTP::Request->new('DELETE', $url);
$request->authorization_basic($cfg->val("GENERAL","apikey"), "account=".$cfg->val("GENERAL","account"));

my $ua = LWP::UserAgent->new();
my $res = $ua->request($request);
die $res->message if $res->is_error;

# EOF 
