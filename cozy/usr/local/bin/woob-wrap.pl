#!/usr/bin/perl
#
# Copyright (c) 2018-2024 Mathieu Roy <yeupou--gnu.org>
#      http://yeupou.wordpress.com
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
#   USA

#
# expect ~/db and ~/data dirs to exists
#   ~/db/app.module/ dir + woob relevant module configured 
#
# downloaded file list is kept in ~/db/app.module/index.json but so far
# we do not cross check if existing files are the one registered, we just assume
# the original file id is enough


use strict;
use File::HomeDir;
use File::Basename;
use File::Temp qw(tempfile :POSIX);
use File::Copy;
use Term::ANSIColor qw(:constants);
use JSON; 
use Digest::MD5::File qw(file_md5_hex);
    
my $dbdir = File::HomeDir->my_home()."/db";
my $outdir = File::HomeDir->my_home()."/data";
my $woobbin = "/usr/bin/woob";
my $valid_apps = "bill";

# run every app+module
while(defined(my $dir = glob("$dbdir/*"))){
    next unless -d $dir;
    my ($app,$module) = split('\.', basename($dir));
    next unless $app =~ /($valid_apps)/;
    my %local_files;
    
    # read current database, if existing
    my $local_json = "$dir/index.json";
    if (-e $local_json) {
	# slurp json
	open(JSON, "< $dir/index.json");
	my $file_json;
	while(<JSON>){ $file_json .= $_; }
	close(JSON);
	my $decoded_json = decode_json($file_json);

	# fill the hash with json data 
	foreach my $file (keys %{$decoded_json}) {
	    $local_files{$file} = $decoded_json->{$file};
	}
    }

    # grab and read online database
    # FIXME : command suitable only for bill backend
    my $list = decode_json(`$woobbin $app -b $module documents --formatter=json -n 1000`);
    my %missing_files;
    foreach my $file (@{$list}) {
	# ignore if id is already known
	next if $local_files{$file->{"id"}};
	# otherwise save with the intended filename base, without suffix
	$missing_files{$file->{"id"}} = fileparse($file->{"label"}, $file->{"type"});
    }

    # download file as temporary, and move to final destination with partial md5 and type
    # (assume PDF at this stage)
    system("mkdir", "$outdir/$module") unless -d "$outdir/$module";

    # download each missing file
    my $updated;
    foreach my $file (keys %missing_files) {
	# download in a tmpfile
	my $tmpfile = tmpnam();
	system($woobbin,
	       $app,
	       "-b", $module,
	       "download",
	       $file,
	       $tmpfile);
	
	# find shortened md5, skip file if failed
	my $md5 = file_md5_hex($tmpfile) or warn "$file md5 failed: $!\n" and next;
	$md5 = substr($md5, 0, 8);

	# define final name
	my $filename = $missing_files{$file}."_".$md5.".pdf";

	# move rename
	move($tmpfile, "$outdir/$module/$filename") or next;

	# if we reach this point, the file was downloaded ok, remove from missing
	# and update local file list
	delete($missing_files{$file});
	$local_files{$file} = $filename;
	$updated++;

    }

    # check if all missing files were handled
    if (scalar(keys %missing_files)) {
	print "error, some files are missing but could not be downloaded properly:\n";
	foreach my $file (keys %missing_files) {
	    print "\t$file ($missing_files{$file})\n";
	}
    }

    # save updated filelist if at least one file was downloaded
    next unless $updated;

    open(JSON, "> $dir/index.json");
    print JSON encode_json \%local_files;
    close(JSON);
    
}

# EOF
