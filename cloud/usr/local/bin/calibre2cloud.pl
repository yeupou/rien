#!/usr/bin/perl
#
# Copyright (c) 2023 Mathieu Roy <yeupou--gnu.org>
#      http://yeupou.wordpress.com
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
#   USA

use strict;
use Getopt::Long;

my $bin_calibredb = "/usr/bin/calibredb";
my @calibredb_opts = ("--automerge", "ignore");
my $filetype = "pdf|epub|azw3|mobi";
my $occ = "/usr/local/share/nextcloud/occ";
my $bin_php = "/usr/bin/php";
my $verbose;

# command line args
my ($getopt,$dir_root, $dir_in,$dir_out);
eval {
    $getopt = GetOptions("root:s" => \$dir_root,
			 "in:s" => \$dir_in,
			 "out:s" => \$dir_out,
			 "verbose" => \$verbose,
	);
};

die unless -d $dir_root;
die unless -d "$dir_root/$dir_in";
die unless -d "$dir_root/$dir_out";

my $change = 0;
while(defined(my $file = glob("$dir_root/$dir_in/*"))){
    next unless -f $file;
    next unless $file =~ /($filetype)$/i;
    print $file."\n" if $verbose;
    system($bin_calibredb,
	   "add",
	   @calibredb_opts,
	   $file);
    unlink($file);
    $change++;
}

if ($change) {
    system($bin_php, $occ, "--quiet", "files:scan", "--path", $dir_in);
    system($bin_php, $occ, "--quiet", "files:scan", "--path", $dir_out
);
}

# EOF
