#!/usr/bin/perl
#
# Copyright (c) 2017 Mathieu Roy <yeupou--gnu.org>
#                   http://yeupou.wordpress.com
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
#   USA

use strict;
use Fcntl qw(:flock);
use POSIX qw(strftime);
use File::HomeDir;

my $binmpc = "/usr/bin/mpc";
my $binnotify = "/usr/bin/notify-send";
my $icon  = "/usr/local/share/utils-mpc-playlist/view-media-playlist.svg.png";
my $filemonitorskip = File::HomeDir->my_home()."/.mpc-monitor-skip";
my $wait = 3;

# silently forbid concurrent runs
# (http://perl.plover.com/yak/flock/samples/slide006.html)
open(LOCK, "< $0") or die "Failed to ask lock. Exit";
flock(LOCK, LOCK_EX | LOCK_NB) or exit;
 
# stop buffering print()
$| = 1;
# loop forever with configured wait time 
my $latest_song;
while (sleep $wait) {
    my $current_song;		      
    open(MPC_CURRENT, "$binmpc current |");
    while (<MPC_CURRENT>) { chomp(); $current_song .= $_; }
    close(MPC_CURRENT);

    # new song being played? notify (unless mpc+notify left a mark that it is not necessary)
    if (! -e $filemonitorskip) {
	system($binnotify, "--icon=$icon", $current_song) if ($current_song ne $latest_song);
    }

    # in any cases, remove any mark left by mpc-notify
    unlink($filemonitorskip) if (-e $filemonitorskip and -f $filemonitorskip);
    
    # always update latest song var
    $latest_song = $current_song;
}


# EOF
