require ["duplicate"];

# discard any message with the same msg-id already received in the two last hours
# (mailing-list duplicates, cross-post etc)
if duplicate :seconds 7200 {
   discard;
   stop;
}

# EOF
