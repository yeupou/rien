#!/usr/bin/perl
#
# Copyright (c) 2023 Mathieu Roy <yeupou--gnu.org>
#        http://yeupou.wordpress.com/
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
#   USA

use strict;
use Getopt::Long;
use Sys::Syslog qw(:standard);
use Fcntl ':flock';
use File::HomeDir;
use File::Basename;
use Term::ANSIColor qw(:constants);

my $bin_doveadm = "/usr/bin/doveadm";
my $bin_chown = "/bin/chown";
my $bin_chmod = "/bin/chmod";
my $bin_mkdir = "/bin/mkdir";

# On this setup, mails are meant to be deleted automatically by expiration.
# When expired, extract mail to mbox for distant archival and immediatly delete from the server.
#
# We do not want to archive junk or manually removed files. Junk files we'll be expired if read
# or if very old if not read.
# 
# Files marked are non expirable, so they wont be archived until the mark is removed

### Expiring rules

# regular file: read and 2 weeks old
my $expire_rule_regular = "SEEN not FLAGGED not DRAFT BEFORE 2w";

# others:
#   - in Trash and read since 3 days
#   - in Trash and unread since 6 days
#   - in Junk read since 1 hour
#   - in Junk unread since 2 days
#  (Junk and Trash are defined in /etc/dovecot/conf.d/15-mailboxes.conf
#   - marked Sysadmin by sieve, not flagged and read since 3 days
#   - marked Sysadmin by sieve,not flagged and unread since 10 days
my @expire_rule_others = ("MAILBOX 'Trash' SEEN BEFORE 3d", 
			  "MAILBOX 'Trash' not SEEN BEFORE 6d",
			  "MAILBOX 'Junk' SEEN BEFORE 1h",
			  "MAILBOX 'Junk' not SEEN BEFORE 2d",
			  "MAILBOX '*' HEADER X-Sieve-Mark Sysadmin not FLAGGED SEEN BEFORE 3d",
			  "MAILBOX '*' HEADER X-Sieve-Mark Sysadmin not FLAGGED not SEEN BEFORE 10d");



### FUNCTIONS
my $debug = 0;
sub debug {
    return unless $debug;
    my $color = RESET;
    $color = BRIGHT_GREEN if $_[1] > 0;
    $color = BRIGHT_YELLOW if $_[1] > 1;
    $color = BRIGHT_BLUE if $_[1] > 9;
    print $color, $_[0], RESET;
}

sub run {
    debug(join(" ", @_)."\n", 10);
    system(@_);
}


### Get options and provide help

my ($help,$getopt,$username,$verbose);
my $destusername = "userssync";
eval {
    $getopt = GetOptions("help" => \$help,
			 "verbose" => \$debug,
                         "username=s" => \$username,
			 "destuser=s" => \$destusername);
};

my $outdir = "/home/$destusername/archiving/$username";

if ($help or !$username) {
    print STDERR <<EOF;
Usage: $0
      
    Extract mail for archiving purpose on expiration in
    $outdir.
    Expires junk/trash.
      
    -h, --help          display this help and exit
    -v, --verbose   
    -u, --username XX   mandatory dovecot username
                        (the script need to run as root)
        --destuser XX   destination username to determine
                        (currently: $destusername)
    
      
EOF
exit(1);
}


### INIT
# silently forbid concurrent runs, per user
my $lock  = "/tmp/dove-archive+expire.$username.lock";
open my $flock, ">", $lock or die "Failed to ask lock. Exit";
flock($flock, LOCK_EX | LOCK_NB) or exit;
openlog(basename($0), 'nofatal', 'mail');

### Junk mail, expire
# (first, to avoid expirable to be extracted)
for (@expire_rule_others) {
    run("$bin_doveadm expunge -u $username $_");
}

### Regular mail, extract and expire
unless (-e $outdir) {
    # create outdir if missing
    run($bin_mkdir, "-p", $outdir); 
    # with proper ownership
    # (mbox will be in read only mode for destuser, that will still be able to remove them
    # since he own the folder)
    run($bin_chown, "$destusername:$destusername", $outdir);
    run($bin_chmod, "2775", $outdir);
}
open(DOVEADM_LIST, "$bin_doveadm search -u $username $expire_rule_regular |");
my $messages = 0;
while (<DOVEADM_LIST>) {
    # check if not handled already
    my ($guid,$uid) = split(/\s/, $_);

    # first extract original messageid and size.physical, because we
    # are not 100% sure the IMAP uid wont be reused for another mail
    open(DOVEADM_INFO, "$bin_doveadm fetch -u $username 'size.physical hdr' mailbox-guid $guid uid $uid |");
    my ($size,$msgid);
    while(<DOVEADM_INFO>) {
	if (/^size.physical\:\s*(.*)$/) { $size = $1; }
	if (/^Message-Id\:\s*\<(.*)\>$/i) {
	    $msgid = $1;
	    $msgid =~ s/[^a-zA-Z0-9]//g;
	}
	last if $size and $msgid;	
    }    
    my $outfile =  "$outdir/".$guid."_".$uid."_".$msgid."_".$size.".mbox";
    debug("found $guid $uid for $msgid ($size)\n");
    next if -e $outfile;

    
    # extract content    
    open(MBOX, "> $outfile"); 
    open(DOVEADM_CONTENT, "$bin_doveadm fetch -u $username text mailbox-guid $guid uid $uid |");
    debug("write $outfile\n");
    my $line = 0;
    while(<DOVEADM_CONTENT>) {
	$line++;
	next if $line < 2; # skip first line, "text:"
	print MBOX $_;
    } 
    close(DOVEADM_CONTENT); 
    close(MBOX); 
    
    # expunge
    run("$bin_doveadm expunge -u $username mailbox-guid $guid uid $uid");

    $messages++;
}
close(DOVEADM_LIST);
syslog('info', "extracted/expunged %s messages for %s", $messages, $username) if $messages > 0;
debug("extracted/expunged $messages messages for $username\n");
    
closelog();
# EOF
