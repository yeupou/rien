SHELL = /bin/bash -O globstar
PREFIX = /
LATESTIS = LATESTIS
MAJORVERSION = 4
VERSION = $(shell cat $(LATESTIS) | head -1)
PREVERSION = $(shell cat $(LATESTIS) | tail -1)
NEWVERSION = $(shell expr $(VERSION) \+ 1)
NEWPREVERSION = $(shell expr $(PREVERSION) \+ 1)
WHOAMI = $(shell whoami)
KEYS = 0
GPG_SIGNATURES = $(shell for key in ` gpg --list-secret-keys --with-colons | grep sec | cut -f 5 -d :`; do echo "-u $$key"; done)
DPKG_OPTS = -b -us -uc

$(eval TEMPDIR := $(shell mktemp --directory)) 

install: clean
	@echo "INSTALL WITH PREFIX "$(PREFIX)

log:
	git log --stat -n100 --pretty=format:"%s of %ad" > ChangeLog

readme:
	debian/makereadme.pl

gpgopts:
	@echo $(GPG_SIGNATURES)

deb-prerelease:
	@echo "New prerelease "$(NEWPREVERSION)" (on top of "$(MAJORVERSION).$(VERSION)")"
	debian/makechangelog.sh $(MAJORVERSION) $(VERSION) $(NEWPREVERSION)

	cd debian && rm -f changelog && ln -s changelog.full changelog
	echo $(VERSION) > $(LATESTIS)
	echo $(NEWPREVERSION) >> $(LATESTIS)
	git commit -a -m 'New prerelease $(NEWPREVERSION) (on top of $(MAJORVERSION).$(VERSION))'
	git push
	make log
	dpkg-buildpackage $(DPKG_OPTS) -rfakeroot


deb-release:
	@echo "New release "$(MAJORVERSION).$(NEWVERSION)
	debian/makechangelog.sh $(MAJORVERSION) $(NEWVERSION)
	cd debian && rm -f changelog && ln -s changelog.releases changelog
	# update the LATESTIS reminder file as if it were a pre release
	# so it increases the commit count
	echo $(VERSION) > $(LATESTIS)
	echo $(NEWPREVERSION) >> $(LATESTIS)
	# build the package early to make sure everything is okay
	dpkg-buildpackage $(DPKG_OPTS) -rfakeroot
	# update at the last minute the LATESTIS reminder file, when no one
	# needs exactly the commit count
	echo $(NEWVERSION) > $(LATESTIS)
	echo 0 >> $(LATESTIS)
	# then commit changes, assuming it worked ok 
	git commit -a -m "`cat debian/changelog  | head -3 | tail -1 | sed s/^\ \ \\\*\ //;` (new release $(MAJORVERSION).$(NEWVERSION))"
	git push
	git push gitlab

test: clean-prev-dir readme deb-prerelease clean

pre: prerelease

prerelease: clean-prev-dir readme deb-prerelease clean move-prepare move-local

rel: release

release: clean-prev-dir readme deb-release clean move-prepare move

norel: move-grab move-sign move

keys:
	$(eval KEYS = 1)
	@echo Will update the keyring package
	touch debian/keys.rebuild
	gpg $(GPG_SIGNATURES) --export > keyring/etc/apt/trusted.gpg.d/rien-`hostname --short`.gpg
	git add keyring/etc/apt/trusted.gpg.d/rien-`hostname --short`.gpg

pxe:
	@echo Will update the pxe package
	touch debian/utils-pxe.rebuild

sshn:
	$(eval SSH = 22222)

clean:
	find . \( -name "#*#" -or -name ".#*" -or -name "*~" -or -name ".*~" \) -exec rm -rfv {} \;
	rm -f backup*
	rm -rf doc-pak
	# remove not updated packages
	if [ -e debian/notupdated ]; then \
		while read package; do \
			rm -vf ../rien-$$package\_*.deb; \
		done < debian/notupdated; \
	fi

clean-prev-dir:
	rm -f ../rien-*.deb ../rien_*.changes ../rien-keyring_* ../rien*.tar.gz ../rien*.dsc ../rien_*.buildinfo

move-grab:
	$(eval TEMPDIR := $(shell mktemp --directory)) 
	cd $(TEMPDIR) && rsync -avz httplocal:~/apt/* .
	# remove apt files
	cd $(TEMPDIR) && rm -f Packages* Release* InRelease*

move-litter:
	# only keep the latest build
	cd $(TEMPDIR) && rm -fv rien_*.deb
	cd ../ && for deb in rien-*.deb; do \
		if [ `echo $$deb | cut -f 1 -d "_"` != "rien" ]; then \
			rm -fv $(TEMPDIR)/`echo $$deb | cut -f 1 -d "_"`*; \
		fi ; \
		cp -v $$deb $(TEMPDIR); \
	done
	# update the keyring only if make was called with 'keys' 
	if [ $(KEYS) != 0 ]; then cd $(TEMPDIR) && rm -f rien-keyring*.deb; fi
	if [ $(KEYS) != 0 ]; then cp ../rien-keyring_$(MAJORVERSION).*.deb $(TEMPDIR)/; fi
	if [ $(KEYS) != 0 ]; then cd $(TEMPDIR) && ln -s rien-keyring_$(MAJORVERSION).*.deb rien-keyring.deb; fi

move-sign:
	# build proper required repository files
	cd $(TEMPDIR) && apt-ftparchive packages . > Packages 
	cd $(TEMPDIR) && apt-ftparchive release . > Release
	cd $(TEMPDIR) && gpg $(GPG_SIGNATURES) --digest-algo SHA512 --clearsign --output InRelease Release
	cd $(TEMPDIR) && gpg $(GPG_SIGNATURES) --digest-algo SHA512 -abs --output Release.gpg Release

move-prepare: move-grab move-litter move-sign

move-local:
	-cd $(TEMPDIR) && rsync -rl --chmod=ug=rw --chmod=o=rwX --delete -e "ssh"  . httplocal:~/apt/
	rm -r $(TEMPDIR)

move:
	-cd $(TEMPDIR) && rsync -rl --chmod=ug=rw --chmod=o=rwX --delete -e "ssh" . httplocal:~/apt/
	-cd $(TEMPDIR) && rsync -rl --chmod=ug=rw --chmod=o=rwX --delete -e "ssh -p 8022" . apt.rien.pl:~/apt/
	rm -r $(TEMPDIR)

rebuild-all:
	touch  debian/rebuild
