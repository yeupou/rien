#!/usr/bin/perl
#
# Copyright (c) 2012-2017 Mathieu Roy <yeupou--gnu.org>
#         http://yeupou.wordpress.com/
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
#   USA

# set up
use strict;
use Getopt::Long;
use Fcntl qw(:flock);
use File::HomeDir;

my $active_profile = "output:analog-stereo";
my $suspend_profile = "off";
my $pactl = "/usr/bin/pactl";
my $notify = "/usr/bin/notify-send";
die "Unable to run $pactl" unless -x $pactl;
my ($debug, $getopt, $help);
eval { $getopt = GetOptions("debug" => \$debug,
			    "help" => \$help); };

# list of souncards always off (product name) 
my %cards_to_ignore; 
$cards_to_ignore{"Barts HDMI Audio [Radeon HD 6800 Series]"} = 1;

# read user config
# for now only allow ignore_card
my $configrc_file = File::HomeDir->my_home()."/.switch-soundrc";
my $configrc_type = {
	'ignore_card' => 'ARRAY',
};
my $configrc = {
	'ignore_card' => [],
};
open(RCFILE, "<", $configrc_file);
while (<RCFILE>) {
    # error wont kill the process, we will only print a warning
    chomp;
    next if (/^[[:space:]]*#/); # comment
    next if (/^[[:space:]]*$/); # empty line
    my ($key, $value) = split("="); # key = value

    $key =~ s/^\s+|\s+$//g; # remove blank space before and after key/value
    $value =~ s/^\s+|\s+$//g;  
    
    unless ($key && $value) {
	# we need both a key and a value
	print "$configrc_file $.: Unrecognized line format\n";
	next;
    }
    
    if (not defined $configrc_type->{$key}) {
	die "$configrc_file $.: $key: unrecognized option\n";
    }
    if ('ARRAY' eq $configrc_type->{$key}) {
	push(@{$configrc->{$key}}, $value);
    } elsif ('BOOL' eq $configrc_type->{$key}) {
	if ($value =~ /(yes|y|1|true)/i) {
	    $configrc->{$key} = 1;
	} elsif ($value =~ /(no?|0|false)/i) {
	    $configrc->{$key} = 0;
	} else {
	    print "$configrc_file $.: unrecognized boolean: $value\n";
	    next;
	}
    } elsif ('STRING' eq $configrc_type->{$key}) {
	$configrc->{$key} = $value;
    } else {
	die "INTERNAL ERROR: $configrc_type->{$key}: " .
	    "type not implemented\n";
    }
}
close(RCFILE);
# add configrc define cards to ignore list 
for (@{$configrc->{'ignore_card'}}) {
    $cards_to_ignore{$_} = 1;
}

if ($help) {
    print "Usage: $0 [OPTIONS]

      --debug          Verbose mode.

Fast switch between two sound card: useful when you alternatively
use headset and power amp.

You can ignore devices by adding them in $configrc_file:
ignore_card = alsa.card_name1
ignore_card = alsa.card_name2

alsa.card_name can be found by running:
$pactl list | grep alsa.card_name

Currently ignored cards:
";
    for (sort keys %cards_to_ignore) {
	print "\t$_\n";
    }

    print "
Author: yeupou\@gnu.org
       http://yeupou.wordpress.com/
";
    exit;
    
}

# at this point forbid concurrent runs
# (http://perl.plover.com/yak/flock/samples/slide006.html)
open(LOCK, "< $0") or die "Failed to ask lock. Exit";
flock(LOCK, LOCK_EX | LOCK_NB) or die "Already running. Exit";

# get list of active cards (it is a switch, there should be only two, put
# the rest in ignore list)
open(LIST_CARDS, "export LC_ALL=C && $pactl list cards |");
my $current_card;
my %cards;
my $already_found_one_active;
while(<LIST_CARDS>) {
    # which one we work on
    $current_card = $1 if /^Card \#(\d)$/i;

    # take into account ignore list
    if (/device.product.name = \"([^\"]*)\"$/i) {
	$cards{$current_card} = $1;
	delete($cards{$current_card}) if $cards_to_ignore{$1};
    }

    next unless $cards{$current_card};
    
    # record the first card we find active
    $already_found_one_active = $current_card if /Active Profile: $active_profile$/i;
}
close(LIST_CARDS);
print "Is active: $already_found_one_active\n" if $debug;

# now proceed to the changes
my $already_set_one_active = 0;
while (my ($card, $name) = each(%cards)){
    print "$card, $name " if $debug;
    my $profile;

    # if it's the one card flagged as already active, set if off
    if ($already_found_one_active eq $card or $already_set_one_active) {
	$profile =  $suspend_profile;
    } else {
	$profile = $active_profile;

	# notify we will activate this one
	system($notify, "--icon=audio-speakers", $name);
	
	# we wont activate more than one
	$already_set_one_active = 1;
    }
    
    # ask pactl to change the profile
    system($pactl, "set-card-profile", $card, $profile);
    print "=> $profile\n" if $debug;
}

# EOF 
