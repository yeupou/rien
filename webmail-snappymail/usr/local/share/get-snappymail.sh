#!/bin/bash
# get new version, if not packaged upstream

echo "version (as 2.12.2)?"
read VERSION

# unzip new version
mkdir -v snappymail-$VERSION
cd snappymail-$VERSION
wget https://github.com/the-djmaze/snappymail/releases/download/v$VERSION/snappymail-$VERSION.zip
unzip snappymail-$VERSION.zip
rm -fv snappymail-$VERSION.zip

# move data to /var/lib
rm -rfv  ../../../../var/lib/snappymail
mv -v data ../../../../var/lib/snappymail
# create include.php to point to /var/lib
echo "<?php
define('APP_DATA_FOLDER_PATH', '/var/lib/snappymail/');
" > include.php

# conf should be in /etc
# for now let symlinks instead


cd ..
rm -vf snappymail
ln -sv snappymail-$VERSION snappymail
# EOF

