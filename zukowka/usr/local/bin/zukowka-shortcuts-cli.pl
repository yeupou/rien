#!/usr/bin/perl
#
# Copyright (c) 2017-2022 Mathieu Roy <yeupou--gnu.org>
#                   http://yeupou.wordpress.com
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
#   USA

use strict;
use feature "switch";  # require for "for when..."
no warnings 'experimental::smartmatch';

my $binmpc = "/usr/bin/mpc";
my $binnotify = "/usr/bin/notify-send";
my $binsystemctl = "/usr/bin/systemctl";

# map of icons, if any
my %icon  = (
    "voldown" =>  "/usr/share/awesome/themes/powerarrow-zukowka/icons/Enlightenment-X/audio-volume-low.png",
    "volup" =>  "/usr/share/awesome/themes/powerarrow-zukowka/icons/Enlightenment-X/audio-volume-high.png",
    "play" =>  "/usr/share/awesome/themes/powerarrow-zukowka/icons/Enlightenment-X/media-seek-forward.png",
    "pause" =>  "/usr/share/awesome/themes/powerarrow-zukowka/icons/Enlightenment-X/media-playback-pause.png",
    "shutdown" =>  "/usr/share/awesome/themes/powerarrow-zukowka/icons/Enlightenment-X/system-shutdown.png", 
    );
# set appropriate icon for command
my $notifyicon = $icon{@ARGV[0]};


# check the command asked for and pass it to mpc (or else)
for (@ARGV[0]) {
    when ("voldown") {
	system($binmpc, "volume", "-2");
	system($binnotify, "--icon=$notifyicon", "--expire-time=200", "volume ↘");
    }
    when ("volup") {
	system($binmpc, "volume", "+2");
	system($binnotify, "--icon=$notifyicon", "--expire-time=200", "volume ↗");
    }    
    when ("pause") {
	system($binmpc, "pause");
	system($binnotify, "--icon=$notifyicon", "--expire-time=200", "pause");	

    }
    when ("play") {
	system($binmpc, "add", "/");
	system($binmpc, "random", "on");
	system($binmpc, "next");
	system($binmpc, "play");
	system($binnotify, "--icon=$notifyicon", "--expire-time=200", "lecture");
	
    }
    when ("shutdown") {
	system($binsystemctl, "poweroff", "-i");
	system($binnotify, "--icon=$notifyicon", "--expire-time=500", "extinction");

    }
    default {
	system($binnotify, "--expire-time=500", "commande incomprise");
    }
}


# EOF
