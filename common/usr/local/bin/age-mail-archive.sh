#!/bin/bash
#
# Copyright (c) 2023 Mathieu Roy <yeupou--gnu.org>
#           http://yeupou.wordpress.com/
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
#   USA

# special flags:
#    NOHTML: not HTML output
#    NOUNCOMPRESS: only create .age archives, ignore current one

PATH=$PATH:/usr/local/bin

# age-wrap is required
[ ! -x `which age-wrap` ] &&  echo "age-wrap not found" && exit 1

# jdupes is required
[ ! -x `which jdupes` ] &&  echo "jdupes not found" && exit 1

# mhonarc is required, unless sent with special NOHTML flag
[ -z "$NOHTML" ] && [ ! -x "`which mhonarc`" ] && echo "mhonarc not found" && exit 1

AGE_EXTRAEXTONDEC=pp

# loop though files/dirlog
for item in *; do
    ## check content
    if [ -d "$item/source" ]; then
	AGEFILE="$item".tbz2.age
	MAINDIR="$item"
	# see rien-61-age.sh
	if [ "$AGE_EXTRAEXTONDEC" != "" ]; then
	    if [[ "$AGEFILE" == *".$AGE_EXTRAEXTONDEC."* ]]; then
                AGEFILE="${AGEFILE/\.$AGE_EXTRAEXTONDEC/}"
	    fi
	fi
	# check if not already just handled (both age and folder present)
	[ "$PREVDIR" == "$MAINDIR" ] && continue
	echo "$MAINDIR directory found (-> $AGEFILE)" green	
    elif [ -e "$item" ] && [ "${item##*.}" == "age" ]; then
	AGEFILE="$item"
	MAINDIR="${item%.*}"
	# ignore if it is not a tar file
	[ "${MAINDIR##*.}" != "tbz2" ] && continue	
	# see rien-61-age.sh
	if [ "$AGE_EXTRAEXTONDEC" != "" ]; then
            if [[ "$item" != *".$AGE_EXTRAEXTONDEC."* ]]; then
		MAINDIR="${MAINDIR%.*}.$AGE_EXTRAEXTONDEC"
            fi
	fi
	# check if not already just handled (both age and folder present)
	[ "$PREVDIR" == "$MAINDIR" ] && continue
	echo "$AGEFILE found (-> $MAINDIR)" green
    else
	# silently ignore: echo "ignore unrelated $item"
	continue
    fi

    ## find out what needs to be done
    # mark last dir handled
    PREVDIR="$MAINDIR"

    SOURCE="$MAINDIR/source"
    LASTRUN="$MAINDIR/.lastrun"
    UNCOMPRESS=
    COMPRESS=
    BUILDHTML=

    # if no dir/source or if dir/lastrun is older than .age,
    # uncompress .age to get updated data from archive and rebuild/build HTML
    [ ! -d "$SOURCE" ] && UNCOMPRESS=1 BUILDHTML=1 && echo "$SOURCE does not exist"
    [ -e "$LASTRUN" ] && [ -e "$AGEFILE" ] && [ "$LASTRUN" -ot "$AGEFILE" ] && UNCOMPRESS=1 BUILDHTML=1 && echo "$LASTRUN is older than $AGEFILE"
    
    # find youngest file in dir/source
    SOURCE_LATEST=
    if [ -d "$SOURCE" ]; then
	for msg in "$SOURCE"/*; do
	    [ ! -f "$msg" ] && continue
	    [ "$msg" -nt "$SOURCE_LATEST" ] && SOURCE_LATEST="$msg"
	done
    fi
    
    # if there is no lastrun, rebuildhtml
    [ ! -e "$LASTRUN" ] && BUILDHTML=1 && echo "$LASTRUN does not exist"
    # if any file in dir/source is younger than dir/lastrun, rebuild/build HTML
    [ -e "$LASTRUN" ] && [ -e "$SOURCE_LATEST" ] && [ "$SOURCE_LATEST" -nt "$LASTRUN" ] && BUILDHTML=1 && echo "$SOURCE_LATEST is younger than $LASTRUN"
    
    # if .age does not exist, create .age
    [ ! -e "$AGEFILE" ] && COMPRESS=1 && echo "$AGEFILE does not exist"
    # if any file in dir/source is younger than .age, recreate .age to put up new data
    [ -e "$AGEFILE" ] && [ -e "$SOURCE_LATEST" ] && [ "$SOURCE_LATEST" -nt "$AGEFILE" ] && COMPRESS=1 && echo "$SOURCE_LATEST is younger than $AGEFILE"

    # possible flags ignore
    [ "$NOHTML" == 1 ] && BUILDHTML=
    [ "$NOCOMPRESS" == 1 ] && COMPRESS=
    [ "$NOUNCOMPRESS" == 1 ] && UNCOMPRESS=
    
    ## actually proceed
        
    # uncompress
    if [ "$UNCOMPRESS" == 1 ]; then
	age-wrap --input="$AGEFILE"
    else
	echo "not necessary to uncompress archive"
    fi

    # rename all input file too avoid keeping dupes or preventing overwriting files (.eml with same title, etc)
    if [ -d "$SOURCE" ]; then
	OLDPWD="$PWD"
	cd "$SOURCE"
	for msg in *; do
	    [ ! -f "$msg" ] && continue
	    ## skip if already begins with md5s    ### deactivated, useless, fast enough
	    #if [ "${file:0:4}" == "md5s" ]; then continue ; fi
	    
	    # rename accordingly, keeping eventual extension
	    md5=($(md5sum "$msg"))
	    ext="${msg##*.}"
	    if [ "$ext" == "$msg" ]; then ext= ; else ext=.$ext ; fi
	    if [ "$msg" != "$md5$ext" ]; then mv -v "$msg" "$md5$ext" ; fi
	done
	cd "$OLDPWD"
    fi
    
    # compress
    if [ "$COMPRESS" == 1 ]; then
	age-wrap --compress --only-subdir=source --input="$MAINDIR" 
    else
	echo "not necessary to recreate archive"
    fi

    # finally build/update HTML
    if [ "$BUILDHTML" == 1 ]; then
	
	# make sure we have an monharc rcfile
	MRC=".mail-archive.mrc" 
	if [ ! -e "$MRC" ]; then
	    echo '<MIMEArgs>
m2h_external::filter; usenameext
</MIMEArgs>
<LANG>
fr_FR
</LANG>
<NOCHECKNOARCHIVE>
<ATTACHMENTDIR>
attachments
</ATTACHMENTDIR>
<ATTACHMENTURL>
attachments
</ATTACHMENTURL>
<TLEVELS>
15
</TLEVELS>

# main index by author
<AUTHSORT>
<IDXFNAME>
0001-authors.html
</IDXFNAME>
<IDXPREFIX>
0001-authors
</IDXPREFIX>

<AUTHORBEGIN>
<li>$FROMNAME$ <em>&lt;$FROMADDR$&gt;</em>
</AUTHORBEGIN>
<LiTemplate>
<ul><li><strong>$SUBJECT$</strong>, $DDMMYYYY$</li></ul>
</LiTemplate>
</li>
<AUTHOREND>
</li>
</AUTHOREND>


# thread index
<TREVERSE>
<TIDXFNAME>
0000-threads.html
</TIDXFNAME>
<TIDXPREFIX>
0000-threads
</TIDXPREFIX>

# add date in the threads list
<TTopBegin>
<li><strong>$SUBJECT$</strong>, $DDMMYYYY$, 
<em>$FROMNAME$</em>
</TTopBegin>
<TLiTxt>
<li><strong>$SUBJECT$</strong>, $DDMMYYYY$, 
<em>$FROMNAME$</em>
</TLiTxt>
<TSingleTxt>
<li><strong>$SUBJECT$</strong>, $DDMMYYYY$, 
<em>$FROMNAME$</em>
</TSingleTxt>' > "$MRC"
	fi

	OLDPWD="$PWD"
	cd "$MAINDIR"
	echo "generate HTML from $MAINDIR/source" yellow
	mhonarc -rcfile ../"$MRC" -quiet \
		-title "${MAINDIR%.*} by Author " -ttitle "${MAINDIR%.*} by Thread" \
		source/*
	cd "$OLDPWD"
    else
	echo "not necessary to rebuild HTML"
    fi
    
    date > "$LASTRUN"
    
done

# make sure we are not filling our hard disk with copies of the same files
# (image mail signature, etc)
jdupes --link-hard --recurse .

# EOF
