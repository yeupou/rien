#!/usr/bin/perl
#
# Copyright (c) 2023 Mathieu Roy <yeupou--gnu.org>
#        http://yeupou.wordpress.com/
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
#   USA

use strict;
use Getopt::Long;
use File::HomeDir;
use File::Basename;
use Cwd;
use File::Find;
use Term::ANSIColor qw(:constants);
use Archive::Tar;

my $bin_tar = "/bin/tar";
my $bin_bunzip2 = "/bin/bunzip2";
my $bin_age = "/usr/bin/age";


### FUNCTIONS
my $debug = 0;
sub debug {
    return unless $debug;
    my $color = RESET;
    $color = BRIGHT_GREEN if $_[1] > 0;
    $color = BRIGHT_YELLOW if $_[1] > 1;
    $color = BRIGHT_BLUE if $_[1] > 9;
    print $color, $_[0], RESET;
}

sub run {
    debug(join(" ", @_)."\n", 10);
    system(@_);
}


### Get options and provide help
my $HOME = File::HomeDir->my_home();
my $PWD = getcwd();
# FIXME: maybe it should be based on inputdir and not PWD
my @rcdir = ($HOME, $PWD);
my $rcfile = ".agerc";
my $clearstring = "pp";
my ($help,$getopt,$verbose, $input, $compress, $onlysubdir);
eval {
    $getopt = GetOptions("help" => \$help,
			 "verbose" => \$debug,
                         "input=s" => \$input,
			 "rcfile=s" => \$rcfile,
			 "compress" => \$compress,
			 "only-subdir=s" => \$onlysubdir,
			 "extra-string=s" => \$clearstring);
};

if ($help or ! $input) {
    print STDERR <<EOF;
    
Usage: $0 --input file|directory
    
    -h, --help              display this help and exit
    -v, --verbose   
        --rcfile  $rcfile   expected to contain AGE_ID=path/id
                            to indicate at least a public key to encrypt
                            (currently: $rcfile in $HOME or $PWD)

    -c, --compress          bzip2 before encrypting
    --only-subdir=regex     to limit files/dirs included when creating tar  
    --extra-string=string   string to add to filename when decrypting or to
                            remove when encrypting, to prevent unvoluntary
                            cloud upload. (default: $clearstring)
    
EOF
exit(1);
}
$compress = COMPRESS_BZIP if $compress;

# on key per file
my @keyfiles;
foreach my $dir (@rcdir) {
    open(RC, "< $dir/$rcfile") or next;
    my $keyfile;
    while(<RC>) {
	chomp();
	$keyfile = $1 if /^AGE_ID=(.*)$/i;
	$keyfile =~ s@^~\/@$HOME/@;
	last if $keyfile;
    }
    close(RC);

    unless ($keyfile) {
	debug("no keyfile found in $dir/$rcfile\n");
	next;
    }
    
    debug("will use $keyfile from $dir/$rcfile\n");
    push(@keyfiles, $keyfile);
}
die "AGE_ID missing from $rcfile, exiting" unless scalar(@keyfiles);

# validate at least one keyfile
my @keys;
foreach my $keyfile (@keyfiles) {
    my $key;
    open(KEY, "< $keyfile") or next;
    while (<KEY>) {
	chomp();
	# take any public key but override with private key if present
	$key = $1 if /^(age1.*)$/;
	if (/^(AGE-SECRET-KEY-.*)$/) {
	    $key = $1;
	    last;
	}    
    }

    unless ($key) {
	debug("no key found in $keyfile\n");
	next;
    }
	    
    debug("will use ".substr($key, 0, 7)."... from $keyfile\n");
    # format it for later use
    push(@keys, "--identity=$keyfile");
}
die "no public or private key found, exiting" unless scalar(@keys);
debug("found ".scalar(@keys)." keys\n");


# require some input
die "$input cannot be found or read" unless -e $input and -r $input;
chdir(dirname($input));
$input = basename($input);

# what needs to be done:
#   - file: file.age: decrypt ; anything other file encrypt
#   - folder: file.tar.age or file.tbz2.age decrypt and unarchive ; folder archive and encrypt

# it is a folder: we need to make a tar archive, possibly compress and
# then possibly encrypt
my @unlink;
if (-d $input) {
    debug("$input is a folder\n");
    # create tar archive
    my $tar = Archive::Tar->new();
    # find all files to include
    my @inventory = ();
    find (sub {
	# specific case, when we want to include only one specific file or subdir
	next if $onlysubdir and $File::Find::name !~ /$onlysubdir/;
	push (@inventory, $File::Find::name)
	  }, $input);
    $tar->add_files(@inventory);

    # define filename that will later be used as input
    unless ($compress) {
	$input .= ".tar";
    } else {
	$input .= ".tbz2";
    }
    $tar->write($input, $compress);
    debug("wrote $input with ".scalar(@inventory)." files\n", 10);
    push(@unlink, $input);
}
# at this point there should be no folder
unless ($input =~ /\.age$/i) {
    # if the file is not an .age, we need to encrypt it now 
    debug("$input is not an age file\n");
    my $output = "$input.age";

    # might need to remove cloud exclude tag
    $output =~ s/\.$clearstring\.?/./ if $clearstring;
    
    # encrypt
    run($bin_age, @keys, "--encrypt", "--output=$output", $input);
    
} else {
    # otherwise we need to decrypt it

    my $output = $input;
    # remove age extension
    $output =~ s/\.age$//i;
    # might need to add cloud exclude tag
    if ($clearstring and $output !~ m/\.$clearstring\.?/) {
	if ($output !~ m/\.\w+$/) {
	    # no extension? add directly
	    $output .= ".$clearstring";
	} else {
	    $output =~ s/(\.[^.]+$)/.$clearstring$1/;
	}
    }
    
    # decrypt

    run($bin_age, @keys, "--decrypt", "--output=$output", $input);
    $input = $output;

    # might require to uncompress to tar
    run($bin_bunzip2, $input) if $output =~ s/\.tbz2$/.tar/i;
    $input = $output;

    # might require to untar
    if ($output =~ s/\.tar$//i) {	
	run($bin_tar, "--extract", "--file=$input", "--one-top-level=$output", "--strip-components=1");
	push(@unlink, $input);
    }
}

# cleanup
unlink(@unlink);


# EOF
