#!/usr/bin/perl
#
# Copyright (c) 2023 Mathieu Roy <yeupou--gnu.org>
#        http://yeupou.wordpress.com/
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
#   USA

use strict;
use Getopt::Long;
use Sys::Syslog qw(:standard);
use Fcntl ':flock';
use POSIX;
use File::HomeDir;
use File::Basename;
use Term::ANSIColor qw(:constants);

# read mbox files from archiving folder in order to produce an YYYYMM.tar.age file ready for 
# age-mail-archive, then ask nextcloud to rescan relevant directories
#
# require as argument input and output directories
# 
# it will build the YYYYMM based on mtime of each mbox, so it might actually create 
# several YYYYMM.tar.age at once.
# But if any mbox has YYYYMM that is not current year and month, it will be removed as soon
# as the relevant YYYYMM.tar.age is built.
#
# it means that:
#    - current month, incrementally build the YYYYMM.tar.age, never erase previous build
#    - any other month, build a definitive YYYYMM.tar.age: it should not happen twice for a same
#    month

# force specific user
my $valid_user = "www-data";
die "restricted to $valid_user user, exiting" unless getpwuid($<) eq $valid_user;

# current month
my $current_month = strftime("%Y%m",localtime time());
my $bin_mv = "/bin/mv";
my $bin_cp = "/bin/cp";
my $bin_mkdir = "/bin/mkdir";
my $bin_tar = "/bin/tar";
my $bin_bzip2 = "/bin/bzip2";
my $bin_age = "/usr/bin/age";


### FUNCTIONS
my $debug = 0;
sub debug {
    return unless $debug;
    my $color = RESET;
    $color = BRIGHT_GREEN if $_[1] > 0;
    $color = BRIGHT_YELLOW if $_[1] > 1;
    $color = BRIGHT_BLUE if $_[1] > 9;
    print $color, $_[0], RESET;
}

sub run {
    debug(join(" ", @_)."\n", 10);
    system(@_);
}

### Nextcloud info

# directly include nextcloud calls
my $bin_occ = "/usr/bin/php --define apc.enable_cli=1 /usr/local/share/nextcloud/occ";
my $datapath = `$bin_occ config:system:get datadirectory`;
chomp($datapath);


### Get options and provide help

my ($help,$getopt,$verbose);
my ($inputdir, $outputdir);
eval {
    $getopt = GetOptions("help" => \$help,
			 "verbose" => \$debug,
                         "input=s" => \$inputdir,
			 "output=s" => \$outputdir);
};

if ($help or !$inputdir or !$outputdir) {
    print STDERR <<EOF;
Usage: $0
    
    Import mbox into age tar archives.
    
    -h, --help          display this help and exit
    -v, --verbose   

    [MANDATORY]
        --input path    user/files/mboxfolder
	--ouput path    user/files/agefolder
    
    Path are relatives to nextcloud datadirectory:
	$datapath
      
EOF
exit(1);
}

### INIT
# silently forbid concurrent runs
my $lock  = "/tmp/age-mail-archive-mbox2age.lock";
open my $flock, ">", $lock or die "Failed to ask lock. Exit";
flock($flock, LOCK_EX | LOCK_NB) or exit;
openlog(basename($0), 'nofatal', 'syslog');

# make sure relevant directories are available
for ("$datapath/$inputdir", "$datapath/$outputdir") {
    unless (-d $_) {
	syslog('info', "%s not found", $_);
	die("$_ not found, exiting");
    }
}

### List available mboxes
chdir("$datapath/$inputdir") or die "Unable to enter $datapath/$inputdir, exiting";
debug("chdir $datapath/$inputdir\n", 1);

opendir(INPUTDIR, "$datapath/$inputdir");
my %message;
my %message_mostrecent;
while (defined(my $mbox = readdir(INPUTDIR))) {
    next if -d $mbox;
    next unless -r $mbox;
    next unless $mbox =~ /\.mbox$/i;
    
    my $mtime = (stat($mbox))[9];
    # need to keep most recent mtime for comparison purpose
    # but otherwise we are only interested in the the form YYYYMM
    my $mtime_basic = strftime("%Y%m",localtime $mtime);
    $message_mostrecent{$mtime_basic} = $mtime if $mtime > $message_mostrecent{$mtime_basic};

    debug("found [$mtime_basic] $mbox\n");
    push(@{ $message{$mtime_basic} }, $mbox);
}
closedir(INPUTDIR);

### Deal with each YYYYDD group
chdir("$datapath/$outputdir") or die "Unable to enter $datapath/$outputdir, exiting";
debug("chdir $datapath/$outputdir\n", 1);
# make sure we have an age public key available
my $age_pubkey = ".age.pub";
unless (-e $age_pubkey) {
    syslog('info', "%s not found", $age_pubkey);
    die("$age_pubkey not found, exiting");
}
chdir("$datapath/$inputdir") or die "Unable to enter $datapath/$outputdir, exiting";
debug("chdir $datapath/$inputdir\n", 1);


my %dir_changed;
for my $month (keys %message) {
    debug("deal with files of month $month\n", 1);
    
    # check if there is already an archive
    my $age_file = "$datapath/$outputdir/$month.tbz2.age";
    my $age_file_mtime = 0;
    if (-e $age_file) {
	$age_file_mtime = (stat($age_file))[9];
	debug("$age_file exists\n");
    } 
    
    # either we have no archive for this month, or it is older than current files
    # in both cases, we need to recreate it
    if ($message_mostrecent{$month} > $age_file_mtime) {
	debug("most recent message is younger than $age_file ($message_mostrecent{$month} vs $age_file_mtime)\n");
	
	# do not proceed if stale tar file already exists
        die $month.".tar exists, exiting" if -e $month.".tar";
	die $month.".tar.bz2 exists, exiting" if -e $month.".tar.bz2";
	
	# add each mbox to the relevant tar (still outside of the outdir)
	for my $mbox (@{ $message{$month} }) {
	    # update/create tar (FIXME: bug when called from system)
	    `$bin_tar --append --transform=s\@\@"$month"/source/\@  --file "../$month.tar" "$mbox"`;
	    debug("$bin_tar --append --transform=s\@\@$month/source/\@ --file ../$month.tar $mbox\n",10);
	}

	# compress
	run($bin_bzip2, "--compress", "--quiet", "--best", "../$month.tar");

	# then encrypt
	run($bin_age, "-R", "$datapath/$outputdir/.age.pub", "--encrypt", "--output", "$datapath/$outputdir/$month.tbz2.age", "../$month.tar.bz2");
	
	# remove tar
	unlink("../$month.tar.bz2");
	
	# record that output changed
	$dir_changed{$outputdir}++;
    }
    
    # now cleanup dir (not earlier, only once maybe necessary tar.age are rebuild
    #  in case it is not the current month
    if ($current_month ne $month) {
	debug("$month is not the current month, remove old mboxes\n");
	for my $mbox (@{ $message{$month} }) {
	    unlink($mbox);
	    $dir_changed{$inputdir}++;
	}
    } 
    
}


### Make sure nextcloud database is up do date

for my $dir (keys %dir_changed) {
    run("$bin_occ --quiet files:scan --path='/$dir'");
}


closelog();


# EOF
