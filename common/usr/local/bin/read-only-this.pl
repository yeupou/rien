#!/usr/bin/perl
#
# Copyright (c) 2021 Mathieu Roy <yeupou--gnu.org>
#      http://yeupou.wordpress.com
#
#   This program is free software; you can redistribute it and/or modify
#   it under the terms of the GNU General Public License as published by
#   the Free Software Foundation; either version 2 of the License, or
#   (at your option) any later version.
#
#   This program is distributed in the hope that it will be useful,
#   but WITHOUT ANY WARRANTY; without even the implied warranty of
#   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#   GNU General Public License for more details.
#
#   You should have received a copy of the GNU General Public License
#   along with this program; if not, write to the Free Software
#   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307
#   USA

# Lot of files stored, that you want to keep as original (pictures, tax bills, etc, maybe on another server accessed
# through samba),
# that you still do not want to take the risk to loose by a simple erroneous rm call or some stupid virus?
# 
# This script will reassign files from directories configured in /etc/read-only-this.conf
# by changing their owner from $user to $dedicatedsuser (for instance $user-ro),
# keeping their group ownership but in read-only access.
# Directories also will be changed and flagged with T (otherwise that would not prevent file deletion)
# Why not changing their owner to root or mark them as immutable?
# To still enable modifications with a non-priviledged user (for instance over samba or in script).
#
# Obviously, this is moot against someone with root access.
#
# Conffile is /etc/read-only-this.conf
# Sections defines the user name and contains path and keywords 1month, 1year, 1week to set how old must be the
# file to allow change
#
#<user-ro>
#        /home/user/thisdir
#        /home/user/DEUX
#        ONE_MONTH
#</user-ro>
#
#<anotheruser-ro>
#        /home/anotheruser/blah
#        ONE_YEAR
#</<anotheruser-ro>



use strict;
use Fcntl ':flock';
use Config::General;
use File::Find::Rule;
use File::MimeInfo;
use File::Basename;
use Time::Piece;
use Time::Seconds;
use File::chmod;
$File::chmod::UMASK = 0;
$File::chmod::DEBUG = 0;

my $t1y = localtime() - ONE_YEAR;
my $t1m = localtime() - ONE_MONTH;
my $t1w = localtime() - ONE_WEEK;

my $debug = 0;

# silently forbid concurrent runs
# (http://perl.plover.com/yak/flock/samples/slide006.html)
open(LOCK, "< $0") or die "Failed to ask lock. Exit";
flock(LOCK, LOCK_EX | LOCK_NB) or exit;

###  Read conf
my $conffile = "/etc/read-only-this.conf";
my $conf = new Config::General(-ConfigFile => $conffile);
my %config = $conf->getall;
my @dirs;
my %dirs_time;
my %dirs_uid;
for (keys %config) {
    print "$_:\n" if $debug;
    # check we have a relevant <user-ro> section
    # FIXME: (in the future, maybe allow custom mimetype conf with a dedicated section)
    if (getpwnam($_) eq undef) {
	print STDERR "$_ user does not exists, related section in $conffile will be ignored\n";
	print STDERR "(ex: adduser --no-create-home --disabled-password --disabled-login --shell /bin/false $_ )\n";
	next;
    }
    my $uid = getpwnam($_);
    
    # content can be directories or 1year/1month/1week timeframe
    my @thisdirs;
    my $time = $t1y->epoch;  # defaut is a year,
    for my $entry (keys %{$config{$_}}) {
	print "$entry\n" if $debug;
	# store time in epoch
	if ($entry eq "ONE_YEAR") {
	    $time = $t1y->epoch;
	    next;
	}
	if ($entry eq "ONE_MONTH") {
	    $time = $t1m->epoch;
	    next;
	}
	if ($entry eq "ONE_WEEK") {
	    $time = $t1w->epoch;
	    next;
	}
	# store existing directories
	if (-d $entry) {
	    push(@thisdirs, $entry);
	    next;
	}
	# not a valid directory or time
	print STDERR "$entry directory not found, parameter of $_ ignored\n";
    }
    
    # rebuild settings for easier handling
    foreach my $dir (@thisdirs) {
	push(@dirs, $dir);
	$dirs_time{$dir} = $time;
	$dirs_uid{$dir} = $uid;
    }
}

## Check preparing command with each directory
my %files_to_update;
foreach my $dir (@dirs) {
    print "handling $dir (min mtime $dirs_time{$dir}, target uid $dirs_uid{$dir})\n" if $debug;

    # find all the .pm files in @INC
    my @files = File::Find::Rule->file()
	->in($dir);
    # loop through the list
    foreach my $file (@files) {
	my $filename = basename($file);
	
	# ignore hidden and backups files
	next if $filename =~ /^\./;
	next if $filename =~ /\~$/;	
	
	# ignore if already associated by wanted user (logic is: change ownership and mode only once)
	next if (stat($file))[4] eq $dirs_uid{$dir};
	
	# ignore if mtime is bigger than asked (more recent)
	next if (stat($file))[9] > $dirs_time{$dir};
	
	# mimetype based selection
	my $mimetype = mimetype($file);

	# all images audio video
	push(@{$files_to_update{$dirs_uid{$dir}}}, $file) && next if $mimetype =~ /image/i;
	push(@{$files_to_update{$dirs_uid{$dir}}}, $file) && next if $mimetype =~ /audio/i;
	push(@{$files_to_update{$dirs_uid{$dir}}}, $file) && next if $mimetype =~ /video/i;
	# all office files / printout
	push(@{$files_to_update{$dirs_uid{$dir}}}, $file) && next if $mimetype =~ /opendocument/i;
	push(@{$files_to_update{$dirs_uid{$dir}}}, $file) && next if $mimetype =~ /openxmlformat/i;
	push(@{$files_to_update{$dirs_uid{$dir}}}, $file) && next if $mimetype =~ /spreadsheet/i;
	push(@{$files_to_update{$dirs_uid{$dir}}}, $file) && next if $mimetype =~ /x\-tex/i;	
	push(@{$files_to_update{$dirs_uid{$dir}}}, $file) && next if $mimetype =~ /postscript/i;
	push(@{$files_to_update{$dirs_uid{$dir}}}, $file) && next if $mimetype =~ /application\/pdf/i;
	push(@{$files_to_update{$dirs_uid{$dir}}}, $file) && next if $mimetype =~ /x\-dvi/i;
	push(@{$files_to_update{$dirs_uid{$dir}}}, $file) && next if $mimetype =~ /msword/i;
	push(@{$files_to_update{$dirs_uid{$dir}}}, $file) && next if $mimetype =~ /ms-excel/i;
	push(@{$files_to_update{$dirs_uid{$dir}}}, $file) && next if $mimetype =~ /ms-word/i;
	push(@{$files_to_update{$dirs_uid{$dir}}}, $file) && next if $mimetype =~ /gnumeric/i;
	push(@{$files_to_update{$dirs_uid{$dir}}}, $file) && next if $mimetype =~ /sun\.xml/i;
	push(@{$files_to_update{$dirs_uid{$dir}}}, $file) && next if $mimetype =~ /text\/csv/i;
	push(@{$files_to_update{$dirs_uid{$dir}}}, $file) && next if $mimetype =~ /rtf/i;
	push(@{$files_to_update{$dirs_uid{$dir}}}, $file) && next if $mimetype =~ /qet-project/i;
	push(@{$files_to_update{$dirs_uid{$dir}}}, $file) && next if $mimetype =~ /scribus/i;
	push(@{$files_to_update{$dirs_uid{$dir}}}, $file) && next if $mimetype =~ /blender/i;
	push(@{$files_to_update{$dirs_uid{$dir}}}, $file) && next if $mimetype =~ /oxps/i;
	# text
	push(@{$files_to_update{$dirs_uid{$dir}}}, $file) && next if $mimetype =~ /text\/plain/i;		
	# emails
	push(@{$files_to_update{$dirs_uid{$dir}}}, $file) && next if $mimetype =~ /message/i;	
	# all compressed files
	push(@{$files_to_update{$dirs_uid{$dir}}}, $file) && next if $mimetype =~ /zip/i;
	push(@{$files_to_update{$dirs_uid{$dir}}}, $file) && next if $mimetype =~ /compressed/i;
	push(@{$files_to_update{$dirs_uid{$dir}}}, $file) && next if $mimetype =~ /x\-tar/i;
	# web files
	push(@{$files_to_update{$dirs_uid{$dir}}}, $file) && next if $mimetype =~ /html/i;
	push(@{$files_to_update{$dirs_uid{$dir}}}, $file) && next if $mimetype =~ /xml/i;
	push(@{$files_to_update{$dirs_uid{$dir}}}, $file) && next if $mimetype =~ /css/i;
	push(@{$files_to_update{$dirs_uid{$dir}}}, $file) && next if $mimetype =~ /javascript/i;
	push(@{$files_to_update{$dirs_uid{$dir}}}, $file) && next if $mimetype =~ /markdown/i;	
	# all ebook files
	push(@{$files_to_update{$dirs_uid{$dir}}}, $file) && next if $mimetype =~ /epub/i;
	push(@{$files_to_update{$dirs_uid{$dir}}}, $file) && next if $mimetype =~ /mobi/i;
	push(@{$files_to_update{$dirs_uid{$dir}}}, $file) && next if $mimetype =~ /comicbook/i;
	# secured
	push(@{$files_to_update{$dirs_uid{$dir}}}, $file) && next if $mimetype =~ /keepass/i;	
	# other
	push(@{$files_to_update{$dirs_uid{$dir}}}, $file) && next if $mimetype =~ /json/i;
	push(@{$files_to_update{$dirs_uid{$dir}}}, $file) && next if $mimetype =~ /makefile/i;
	push(@{$files_to_update{$dirs_uid{$dir}}}, $file) && next if $mimetype =~ /\.kml/i;
	push(@{$files_to_update{$dirs_uid{$dir}}}, $file) && next if $mimetype =~ /changelog/i;
	push(@{$files_to_update{$dirs_uid{$dir}}}, $file) && next if $mimetype =~ /x\-log/i;

	# always ignore (out of scope so far, but that might be configurable later)
	next if $mimetype =~ /application\/octet-stream/i;
	next if $mimetype =~ /application\/x-ms-shortcut/i;
	next if $mimetype =~ /application\/x-ms-dos-executable/i;
	next if $mimetype =~ /application\/x-sharedlib/i;	
	next if $mimetype =~ /perl/i;		
	next if $mimetype =~ /php/i;	
	next if $mimetype =~ /shellscript/i;
	next if $mimetype =~ /python/i;

	# video related but subject to change
	next if $mimetype =~ /x\-subrip/;

	# unknown files
	print "$filename ignored due to mimetype ".mimetype($file)."\n" if $debug;

    }
}

# Actually modify ownership
# (not earlier it is easier to understand what is being done)

for my $uid (keys %files_to_update) {
    # first files chmod, remove write permission from group and other
    # this assumes that user got write permission and group read permission
    # (the script won't grant write permissions over files)
    chmod("o-w,g-w", @{$files_to_update{$uid}});
    
    # directories containing these files must also be updated once
    # with T flag (that will ignore group ownership for file deletion)
    my %to_ignore;
    my @dirs_to_update;
    foreach my $file (@{$files_to_update{$uid}}) {
	my $dir = dirname($file);
	next if $to_ignore{$dir};
	push(@dirs_to_update, $dir);
	$to_ignore{$dir} = 1;
    }
    # in this case we need to make sure group has write permission, to be able to add new files
    # and T sticky bit (only the owner of the file within that directory,
    # the owner of the directory or the root user to delete or rename the file)
    chmod("o-w,g+w,+t", @dirs_to_update);
    
    # then chown files and dirs (Beware, no effect if not run by root)
    chown($uid, -1, @{$files_to_update{$uid}}, @dirs_to_update);
}

# EOF
