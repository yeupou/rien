# skip already sourced
[ "`declare -f Y61-sourced 2>/dev/null`" ] && return

# set marker to prevent file to be sourced twice
function Y61-sourced() {
    echo true;
}

# skip if age-wrap isnt installed - outdated, backward compat
[ ! -x "`which age-wrap`" ] && return


# outdated, kept for backward compatibility
function Y61-verbose {
    echo "$1"
}


# function encrypt a single file or folder
function age-enc {
    age-wrap "$1"
}

# decrypt a single file
function age-dec {
    age-wrap "$1"
}


# decrypt all age files in current directory
function age-deploy {
    for age in *.age; do
	age-wrap "$age"
    done
}

# encrypt all files within directory, by default PDF, and remove original
# (take file extension as argument)
function age-employ () {
    ext=$1
    [ -z "$ext" ] && ext=pdf
    for file in *.$ext; do
	age-wrap "$file" && rm "$file"
    done
}


# EOF
